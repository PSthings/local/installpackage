$computername = Get-Content .\servers.txt
$sourcefile = "\\server01\pathto\setup.exe"
$credentials = "get=credential"
#This section will install the software 
foreach ($computer in $computername) 
{
    $destinationFolder = "\\$computer\C$\Temp"
    #It will copy $sourcefile to the $destinationfolder. If the Folder does not exist it will create it.

    if (!(Test-Path -path $destinationFolder))
    {
        New-Item $destinationFolder -Type Directory
    }
    Copy-Item -Path $sourcefile -Destination $destinationFolder
    Invoke-Command -ComputerName $computer -ScriptBlock {Start-Process 'c:\temp\setup.exe' -ArgumentList '/s','/v','/qn' - Wait -PassThru } -Credential $Credential
}
if ($setup.exitcode -eq 0) {
    $result = "The Installation of DN is Successful"
       }
       else
       {
    $result = "The Installation of DN is Failed"
       }
   write-host $result
   #Output the install result to the Local D Drive
    Out-File -FilePath \\serv\pathto\result.txt -Append -InputObject ("ComputerName: $computerName Result: $result")
   }
